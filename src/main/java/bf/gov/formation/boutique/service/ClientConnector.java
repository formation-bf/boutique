package bf.gov.formation.boutique.service;

import bf.gov.formation.boutique.config.RemoteClientAccess;
import bf.gov.formation.boutique.dto.ClientDtoList;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Component
@RequiredArgsConstructor
@Slf4j
public class ClientConnector  extends RemoteClientAccess {

    /**
     * Recuperer la liste des clients.
     *
     * @return Liste des clients.
     */
    public ClientDtoList getClients()  {


        log.info("Connexion à l'application client");
        String endpoint = "http://localhost:8081/service/api/formation/liste-client";
        log.info("onnexion à l'application client ... {} ", endpoint);
        try {
            HttpHeaders headers = new HttpHeaders();
            //this.setApiKeyHeader(headers);
            HttpEntity<String> entity = new HttpEntity<>(null, headers);

            ResponseEntity<ClientDtoList> response
                    = super.getRestTemplate().exchange(
                    endpoint,
                    HttpMethod.GET,
                    entity,
                    ClientDtoList.class);

            return response.getBody();
        }catch (HttpServerErrorException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
