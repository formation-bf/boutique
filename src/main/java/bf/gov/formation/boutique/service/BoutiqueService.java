package bf.gov.formation.boutique.service;

import bf.gov.formation.boutique.database.entity.Achats;
import bf.gov.formation.boutique.database.entity.Article;
import bf.gov.formation.boutique.database.repository.AchatsRepository;
import bf.gov.formation.boutique.database.repository.ArticleRepository;
import bf.gov.formation.boutique.dto.AchatsDto;
import bf.gov.formation.boutique.dto.ArticleDto;
import bf.gov.formation.boutique.dto.ClientDto;
import com.github.dozermapper.core.Mapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class BoutiqueService {

    private final Mapper mapper;
    private final AchatsRepository achatsRepository;
    private final ArticleRepository articleRepository;
    private final ClientConnector clientConnector;

    /**
     * retourne nune liste de client.
     * @return liste
     */
    public List<ClientDto> fetchListClient() {
         return clientConnector.getClients().getDatas();
//        return Arrays.asList(
//                new ClientDto("f0f6e4a6-6cf8-11ee-b962-0242ac110002", "SAWADOGO", "Andre", "07/01/2000"),
//                new ClientDto("f0f6e4a6-6cf8-11ee-b962-0242ac120002", "OUEDRAOGO", "Moussa", "12/08/2000"),
//                new ClientDto("f0f6e4a6-6cf8-11ee-b962-0242ac130002", "SANOU", "Malik", "12/11/2000")
//        );
    }


    HashMap<String, ClientDto> getClientMap() {
        HashMap<String, ClientDto> clients =  new HashMap<>();
        for (ClientDto c : clientConnector.getClients().getDatas()) {
            clients.put(c.getNumero(), c);
        }
        return clients;
    }

    /**
     * retourne la liste des achats.
     * @param client
     * @return liste des achats
     */
    public List<AchatsDto> fetchListAchats(final String client) {
        HashMap<String, ClientDto> clients = getClientMap();
        ClientDto clientDto = clients.get(client);
        List<AchatsDto> achatsDtos = this.achatsRepository.findByClient(client).stream()
                .map(c -> {
                    AchatsDto achatsDto = mapper.map(c, AchatsDto.class);
                    achatsDto.setNom(clientDto.getNomClient());
                    achatsDto.setPrenom(clientDto.getPrenomClient());
                    achatsDto.setDateNaissance(clientDto.getDateNaissance());
                    return achatsDto;
                })
                .collect(Collectors.toList());
      return achatsDtos;
    }

    /**
     * retourne nune liste des articles.
     * @return liste des articles
     */
    public List<ArticleDto> fetchListArticles() {
        return this.articleRepository.findAll().stream()
                .map(a -> mapper.map(a, ArticleDto.class))
                .collect(Collectors.toList());
    }


    /**
     * Creer un nouvel achat
     * @param achatsDto
     * @return
     */
    public AchatsDto createAchats(AchatsDto achatsDto) {
        Achats achats = mapper.map(achatsDto, Achats.class);
        achats = this.achatsRepository.saveAndFlush(achats);
        if (!achatsDto.getDatas().isEmpty()) {
            Achats finalAchats = achats;
            List<Article> articles = achatsDto.getDatas().stream()
                    .map(articleDto -> Article.builder()
                            .achat(finalAchats)
                            .categorie(articleDto.getCategorie())
                            .referenceArticle(articleDto.getReferenceArticle())
                            .idArticle(articleDto.getIdArticle()).build())
                    .collect(Collectors.toList());
            articles = this.articleRepository.saveAll(articles);
            achats.setDatas(articles);
        }
        HashMap<String, ClientDto> clients = getClientMap();
        ClientDto client = clients.get(achats.getClient());
        AchatsDto dto = mapper.map(achats, AchatsDto.class);
        dto.setNom(client.getNomClient());
        dto.setPrenom(client.getPrenomClient());
        dto.setDateNaissance(client.getDateNaissance());
        return dto;
    }
}
