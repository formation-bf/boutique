package bf.gov.formation.boutique.database.repository;

import bf.gov.formation.boutique.database.entity.Achats;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AchatsRepository extends JpaRepository<Achats, Long> {
    /**
     * Liste des achats en fonction client
     * @param client : client
     * @return liste
     */
    List<Achats> findByClient(String client);
}
