package bf.gov.formation.boutique.database.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Entite de gestion des achats.
 * @author yigalpen
 * @since oct 2023
 */

@Entity
@Table(name = "achat")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Achats implements Serializable {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "achat_seq_generator")
    @SequenceGenerator(name = "achat_seq_generator",
            sequenceName = "achat_seq",
            initialValue = 100, allocationSize = 1)
    @Column(name = "id")
    private Long idAchat;

    @Column(name = "client")
    private String client;

    @Column(name = "horaire")
    private LocalDate heureAchat;

    @Column(name = "montant")
    private BigDecimal montant;

    @OneToMany(mappedBy = "achat")
    private List<Article> datas;
}
