package bf.gov.formation.boutique.database.repository;

import bf.gov.formation.boutique.database.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Long> {
}
