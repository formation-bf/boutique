package bf.gov.formation.boutique.database.entity;

import bf.gov.formation.boutique.utilities.ECategorie;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Entite de gestion des articles.
 * @author yigalpen
 * @since oct 2023
 */

@Entity
@Table(name = "article")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Article implements Serializable {
    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "article_seq_generator")
    @SequenceGenerator(name = "article_seq_generator",
            sequenceName = "article_seq",
            initialValue = 100, allocationSize = 1)
    @Column(name = "id")
    private Long idArticle;

    @Column(name = "libelle")
    private String referenceArticle;

    @ManyToOne
    @JoinColumn(name = "achat", referencedColumnName = "id")
    private Achats achat;

    @Enumerated(EnumType.STRING)
    @Column(name = "categorie")
    private ECategorie categorie;
}
