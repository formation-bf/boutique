package bf.gov.formation.boutique.config;

import bf.gov.formation.boutique.database.entity.Achats;
import bf.gov.formation.boutique.database.entity.Article;
import bf.gov.formation.boutique.dto.AchatsDto;
import bf.gov.formation.boutique.dto.ArticleDto;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.github.dozermapper.core.loader.api.BeanMappingBuilder;
import com.github.dozermapper.core.loader.api.TypeMappingOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * This class is used to configure the Dozer.
 */
@Configuration
public class DozerConfig {

    private final BeanMappingBuilder builder = new BeanMappingBuilder() {
        @Override
        protected void configure() {
            mapping(Achats.class, AchatsDto.class, TypeMappingOptions.mapNull(false));

            mapping(Article.class, ArticleDto.class, TypeMappingOptions.mapNull(false))
                    .fields("achat.idAchat", "idAchat");


        }
    };


    /**
     * builds the dozer mapper.
     *
     * @return Mapper
     */
    @Bean
    public Mapper buildDozerMapper() {
        return DozerBeanMapperBuilder.create().withMappingBuilder(builder).build();
    }
}
