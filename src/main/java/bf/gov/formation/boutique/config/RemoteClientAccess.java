package bf.gov.formation.boutique.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Slf4j
@Getter
public class RemoteClientAccess {

    @Autowired
    private RestTemplate restTemplate;

    /**
     * creation d'une instande de restemplate.
     * @return RestTemplate
     */
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
