package bf.gov.formation.boutique.dto;

import bf.gov.formation.boutique.utilities.Estatut;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientDto {
    /**
     * numero du client.
     */
    @EqualsAndHashCode.Include
    private String numero;
    /**
     * nom du client.
     */
    private String nomClient;
    /**
     * prenom du client.
     */
    private String prenomClient;
    /**
     * date de naissance.
     */
    private String dateNaissance;
}
