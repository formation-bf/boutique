package bf.gov.formation.boutique.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Entite de gestion des achats.
 * @author yigalpen
 * @since oct 2023
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class AchatsDto implements Serializable {

    @EqualsAndHashCode.Include
    private Long idAchat;

    private String client;

    private String nom;

    private String prenom;

    private String dateNaissance;

    private LocalDate heureAchat;

    private BigDecimal montant;

    private List<ArticleDto> datas;
}
