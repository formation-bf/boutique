package bf.gov.formation.boutique.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Dto de client.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientDtoList implements Serializable {
    private List<ClientDto> datas = new ArrayList<>();
}
