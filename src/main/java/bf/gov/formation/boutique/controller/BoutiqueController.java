package bf.gov.formation.boutique.controller;

import bf.gov.formation.boutique.dto.AchatsDto;
import bf.gov.formation.boutique.dto.ArticleDto;
import bf.gov.formation.boutique.dto.ClientDto;
import bf.gov.formation.boutique.service.BoutiqueService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping(value = "/commerce")
@RequiredArgsConstructor
public class BoutiqueController {

    private final BoutiqueService boutiqueService;


    /**
     * Recupere la liste des clients.
     * @return clients
     */
    @GetMapping("/clients")
    public ResponseEntity<List<ClientDto>> fetchListClient() {
        return new  ResponseEntity<>(this.boutiqueService.fetchListClient(), HttpStatus.OK);
    }


    /**
     * Recupere la liste des clients.
     * @return clients
     * @param client
     */
    @GetMapping("/achats/{client}")
    public ResponseEntity<List<AchatsDto>> fetchListAchats(
            @PathVariable("client") final String client
            ) {
        return new  ResponseEntity<>(this.boutiqueService.fetchListAchats(client), HttpStatus.OK);
    }

    /**
     * Recupere la liste des clients.
     * @return clients
     */
    @GetMapping("/articles")
    public ResponseEntity<List<ArticleDto>> fetchListArticles() {
        return new  ResponseEntity<>(this.boutiqueService.fetchListArticles(), HttpStatus.OK);
    }


    /**
     * Recupere la liste des clients.
     * @return clients
     */
    @PostMapping("/achats")
    public ResponseEntity<AchatsDto> createAchats(
            @RequestBody final AchatsDto achatsDto
    ) {
        return new  ResponseEntity<>(this.boutiqueService.createAchats(achatsDto), HttpStatus.OK);
    }
}
