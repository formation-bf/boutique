package bf.gov.formation.boutique.utilities;

public enum Estatut {
    ACTIF, INACTIF
}
